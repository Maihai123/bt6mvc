﻿using System.ComponentModel.DataAnnotations;

namespace MaiHai.Models
{
    public class Employees
    {
        public int EmployeesId { get; set; }
        [Required, StringLength(100)]
        public string FirstName { get; set; }
        [Range(0.01, 10000.00)]
        public string LastName { get; set; }
        [Range(0.01, 10000.00)]
        public decimal ContactandAddress { get; set; }
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public int AddTextHere { get; set; }
    }
}
